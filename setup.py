import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="WhosWho",
    version="0.7",
    author="Yvan Masson",
    author_email="yvan@masson-informatique.fr",
    description="Create PDF documents with people pictures and corresponding "
                "name, also called \"trombinoscope\"",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://framagit.org/Yvan-Masson/WhosWho",
    scripts=["WhosWho/whoswho"],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 or later "
        "(GPLv3+)",
        "Operating System :: POSIX :: Linux",
        "Development Status :: 4 - Beta",
        "Environment :: X11 Applications :: Gnome",
        "Intended Audience :: End Users/Desktop",
        "Intended Audience :: Education",
        "Topic :: Education"
    ],
    python_requires='>=3.6',
    install_requires=["chardet", "opencv-python-headless", "PyGObject", "Willow"],
    data_files=[
        ("share/whoswho/",
         ["WhosWho/data/default_face_4x5.jpg"]),
        ("share/applications/",
         ["WhosWho/data/whoswho.desktop"]),
        #("share/metainfo/",
        # ["org.framagit.yvanmasson.WhosWho.metainfo.xml"]),
        ("share/icons/hicolor/scalable/apps/",
         ["WhosWho/data/icons/hicolor/scalable/apps/whoswho.svg"]),
        ("share/icons/hicolor/16x16/apps/",
         ["WhosWho/data/icons/hicolor/16x16/apps/whoswho.png"]),
        ("share/icons/hicolor/24x24/apps/",
         ["WhosWho/data/icons/hicolor/24x24/apps/whoswho.png"]),
        ("share/icons/hicolor/32x32/apps/",
         ["WhosWho/data/icons/hicolor/32x32/apps/whoswho.png"]),
        ("share/icons/hicolor/48x48/apps/",
         ["WhosWho/data/icons/hicolor/48x48/apps/whoswho.png"]),
        ("share/icons/hicolor/64x64/apps/",
         ["WhosWho/data/icons/hicolor/64x64/apps/whoswho.png"]),
        ("share/icons/hicolor/96x96/apps/",
         ["WhosWho/data/icons/hicolor/96x96/apps/whoswho.png"]),
        ("share/icons/hicolor/128x128/apps/",
         ["WhosWho/data/icons/hicolor/128x128/apps/whoswho.png"]),
        ("share/icons/hicolor/192x192/apps/",
         ["WhosWho/data/icons/hicolor/192x192/apps/whoswho.png"]),
        ("share/icons/hicolor/256x256/apps/",
         ["WhosWho/data/icons/hicolor/256x256/apps/whoswho.png"])
    ]
)
